api = 2

core = 7.32

projects[admin_menu] = 3.0-rc4
projects[backup_migrate] = 2.8
projects[ckeditor] = 1.16
projects[ctools] = 1.4
projects[devel] = 1.5
projects[file_entity] = 2.0-beta1
projects[google_analytics] = 1.4
projects[libraries] = 2.2
projects[media] = 2.0-alpha4
projects[menu_block] = 2.4
projects[metatag] = 1.4
projects[module_filter] = 1.8
projects[pathauto] = 1.2
projects[pathologic] = 2.12
projects[token] = 1.5
projects[views] = 3.8
projects[webform] = 4.1

libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.5/ckeditor_4.4.5_full.tar.gz"
